/**
 * Actions 
 * 
 * Each function returns a pure object called an action in Redux
 */


export const storeId = (id) => ({
  type: "STOREID",
  id: id,
});

export const changeCurrent = (current) => ({
  type: "CHANGE",
  current: current,
});

export const englishDone = () => ({
  type: "ENGLISH",
});

export const frenchDone = () => ({
  type: "FRENCH",
});

export const choixLangue = (choix) => ({
  type: "CHOIX",
  choix: choix,
});

export const currentTest = (test) => ({
  type: "TEST",
  test:test,
});

export const sendReponses = (responses) => ({
  type: "SENDRESPONSES",
  responses : responses,
})