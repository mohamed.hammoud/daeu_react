import React from 'react';
import { Redirect } from 'react-router-dom'
import "../css/Accueil.css"
import logo from "../images/logo.jpg"
import axios from 'axios';
import { WRONG_CODE } from "./names.js"
import { API } from "./names"
import { changeCurrent } from "../actions/index";
import { connect } from "react-redux";

class Accueil extends React.Component {
    constructor(props) {
        super(props);
        

    this.state = {
        requestPending: false,
        nbAttemptsFail: 0,
        codeValue: "",
        codeIsValid: false,
        questions: []
    }


    
    this.handleSubmit = this.handleSubmit.bind(this)
    // this.navigate = this.navigate.bind(this)
    }

    handleSubmit(e, code) {
        e.preventDefault()
        if (this.state.requestPending) return
        this.setState({ requestPending: true })

        axios.post(API, { type: 'verif_code', 'code': code }, { withCredentials: true })
            .then(response => response.data)
            .then(response => {
                this.setState({
                    nbAttemptsFail: this.state.nbAttemptsFail + 1,
                    codeIsValid: response.validity,
                    requestPending: false
                })
            })
            .catch(error => {
                this.setState({
                    requestPending: false
                })
            })

    }

   

    componentDidMount() {
    
    }

    // navigate(event)  {
    //     const {dispatch} = this.props
    //     event.preventDefault();
    //     const newCurrent = event.target.value - 1;
    //     console.log(newCurrent);
    //     dispatch(changeCurrent(newCurrent))
       
    //   }

      

    render() {
        const { nbAttemptsFail, codeValue, codeIsValid, requestPending} = this.state
        return  (


            <div className="container align">
                
                    {/* {questions.map(question => (


                        <button value={question.id_question} onClick={this.navigate} >{question.id_question}</button>


                    ))} */}

              
                {codeIsValid ? <Redirect to="/formulaire" /> : null}
                <div className="row align-items-center">
                    <div className="col-md-6">
                        <img src={logo} alt="" />
                    </div>
                    <div className="col-md-6">

                        <form onSubmit={(e) => this.handleSubmit(e, codeValue)} >
                            <div className="form-group">
                                <label htmlFor="enterCode">Entrez le code fourni par un encadrant</label>
                                <input
                                    value={codeValue}
                                    onChange={(e) => {
                                        if (!requestPending)
                                            this.setState({
                                                codeValue: e.target.value
                                            })
                                    }}
                                    type="text"
                                    spellCheck="false"
                                    className="form-control"
                                    autoComplete="off"
                                    required
                                />
                                {nbAttemptsFail > 0 & !requestPending ? <span className="help-block">{WRONG_CODE}</span> : ""}
                            </div>

                            <div className="form-group">
                                <button type='submit' className="btn btn-primary">
                                    {requestPending ? <span className="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> : "Valider"}
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        ) 
    } 

    
}

// export default connect(null,null)(Accueil)
export default Accueil;


