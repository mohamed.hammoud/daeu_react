import React from 'react';
import Accueil from './Accueil';
import Formulaire from './Formulaire';
import ChoixTest from './ChoixTest';
import Test from './Test';
import Score from './Score';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'



function App() {
  return (
    <Router>
      <Switch>
        <Route exact path="/">
            <Accueil/>
        </Route>
        <Route exact path="/formulaire">
          <Formulaire/>
        </Route>
        <Route exact path="/Choix">
          <ChoixTest/>
        </Route>
        <Route exact path="/TestAnglais">
          <Test langue ="Anglais"/>
        </Route>
        <Route exact path="/TestFrancais">
          <Test langue="Francais"/>
        </Route>
        <Route exact path="/Score">
          <Score/>
        </Route>
      </Switch>
    </Router>
  );
}

export default App;
