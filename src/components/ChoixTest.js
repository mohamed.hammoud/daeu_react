import '../../node_modules/bootstrap/dist/css/bootstrap.min.css';
import React from 'react';
import { connect } from "react-redux";
import { useState, useEffect } from "react";
import { Redirect } from 'react-router-dom'
import { currentTest } from "../actions/index";
import { CONSIGNE } from './names';
import '../css/ChoixTests.css';







export const ChoixTest = ({ dispatch,englishDone = false, frenchDone = false, choix }) => {

  const [english, setEnglish] = useState(false);
  const [french, setFrench] = useState(false);
  const [score, setScore] = useState(false);

  function Anglais() {
    setEnglish(true) //So that the english button takes us to the english exam
    dispatch(currentTest("Anglais")) //Pour savoir dans quel test on est pour pouvoir changer le store pour enlever les buttons 
                                    //pour les tests finis (voir QCM)
  }

  function Francais() { //idem
    setFrench(true)
    dispatch(currentTest("Francais"))
    
  }

  function Score() {
    setScore(true)
  }





   //Ajout de props langue a Test a cause du probleme de choix (voir App)
  return (
    <div>
      <div className="boutton">
        {english ? <Redirect to="/TestAnglais" /> : null}
        {french ? <Redirect to="/TestFrancais" /> : null}
        {score ? <Redirect to="/Score" /> : null}
        {englishDone === false && choix === "Anglais" ? <button onClick={Anglais} className="btn button-primary boutonAng">Test Anglais</button> : null}
        {frenchDone === false && choix === "Anglais" ? <button onClick={Francais} className="btn button-primary boutonFr">Test Francais</button> : null}
        {frenchDone === false && choix === "Francais" ? <button onClick={Francais}className="btn button-primary boutonFr">Test Francais</button> : null}
        {frenchDone === true && choix === "Francais" ? <button onClick={Score} >Voir vos resultats</button> : null}
        {englishDone === true && frenchDone === true ?
          <button onClick={Score} className="btn button-primary">Voir vos resultats</button> : null}
      </div>
      
      {(!englishDone || !frenchDone) && 
      <div>
       <div className="titreConsigne">Consigne :</div>
      <div className="consigne">{CONSIGNE}</div> </div>} 
    </div>
  
  );


}


const mapStateToProps = (state) => {

  return {
    englishDone: state.englishDone,
    frenchDone: state.frenchDone,
    choix: state.choix
  }
};

export default connect(mapStateToProps, null)(ChoixTest);
