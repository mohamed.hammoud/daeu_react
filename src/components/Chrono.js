import React from 'react';

export class Chrono extends React.Component{

    constructor(props){
        super(props)
        const endDate = Date.now() + (this.props.time * 60 * 1000)
        this.state = {
            endTS : endDate , 
            formatedTimeLeft: null,
            intervalId : null
        };
    }


    componentDidMount() {
        /*timeChrono = document.getElementById("duree").innerHTML * 60;*/
        this.setState({ intervalId: setInterval(() => this.voirTemps(), 1000)})
        //setTimeout( function(){document.getElementById("form").submit()}, timeChrono * 1000);
     }


     formatDate(ts){
        const date = new Date(ts);
        const hours = date.getUTCHours();
        const minutes = "0" + date.getMinutes();
        const seconds = "0" + date.getSeconds();
        return hours + ':' + minutes.substr(-2) + ':' + seconds.substr(-2);
     }

     voirTemps(){
        const {endTS, intervalId} = this.state
        if (endTS - Date.now() <= 0){
            clearInterval(intervalId) 
            this.props.endFunc()
        }else{ 
            const date = new Date(this.state.endTS - Date.now());
            this.setState({ formatedTimeLeft : this.formatDate(date)})
            }
        }

    render() {
        return(
            <div className="container tpsRestant">
                <div id="clockdiv">
                    <h3>Temps restant</h3>
                    {this.state.formatedTimeLeft !== null && <div>{this.state.formatedTimeLeft}</div>}
                </div>
            </div> 


        )

    }
}

export default Chrono;