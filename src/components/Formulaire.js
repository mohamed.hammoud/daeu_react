import React from 'react';
import '../css/Formulaire.css'
import { Form, Field } from "react-final-form";
import axios from 'axios'
import { connect } from "react-redux";
import { choixLangue,storeId } from "../actions/index";
import { FormulaireInformations } from "./formulaireComponents/FormulaireInformations";
import {FormulaireRegime } from "./formulaireComponents/FormulaireRegime";
import { Redirect } from 'react-router-dom'
import {API} from './names'


class Formulaire extends React.Component {
    state = {
        libellesOptions: [],
        creneauxOptions: [],
        requestLanguePending: false,
        requestFormPending: false,
        formulaireDone: false
    }
    onSubmit = this.onSubmit.bind(this)


    onSubmit(e) {
        const { dispatch } = this.props; 
        if (this.state.requestFormPending) return
        e.type = "insertInformations"
        e.indispo = "none" // créneaux
        e.token = this.generateToken()
        dispatch(storeId(e.token))
        dispatch(choixLangue(e.langue)) //Dispatch the choice for button generation
        this.setState({ requestFormPending: true })
        axios.post(API, e, { withCredentials: true })
            .then(response => response.data)
            .then(response => {
                this.setState({ 
                    requestFormPending: false,
                    formulaireDone: true 
                   
                })
            })
            .catch(error => {
                this.setState({ requestFormPending: false })
            })
    }

    generateToken() {
        return Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15)
    }


    render() {
        const { libellesOptions, requestFormPending, requestLanguePending, formulaireDone } = this.state
        return (

            <div>
                {formulaireDone ? <Redirect to="/Choix" /> : null}
                <h1 id="titre">Inscription</h1>
                <Form
                    onSubmit={this.onSubmit}
                    render={
                        ({ handleSubmit }) => (
                            <form id='inscription' className="form container row" onSubmit={handleSubmit}>
                                <div className="col-md-12 text-center">
                                    <FormulaireInformations></FormulaireInformations>
                                    <FormulaireRegime loaded={!requestLanguePending} libelles={libellesOptions} ></FormulaireRegime>
                                    <Field
                                        name='dsi'
                                        component='input'
                                        type='checkbox'
                                        required
                                    />
                                    En soumettant ce formulaire, j'accepte que les informations saisies soient utilisées uniquement dans le cadre du diplôme. < br/>
                                    <button disabled={requestLanguePending} className="btn btn-primary confirmForm" type="submit" >
                                        {requestFormPending ? <span className="spinner-border" role="status" aria-hidden="true"></span> : "Suivant"}
                                    </button>
                                </div>


                            </form>
                        )}
                />


            </div>
        )
    }
}



export default connect(null, null) (Formulaire);