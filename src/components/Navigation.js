import React from 'react';
import { useEffect } from "react";
import { connect } from "react-redux";
import { changeCurrent } from "../actions/index";
import '../css/Test.css';
import {TYPE} from './names';

export const App = ({dispatch, range, qcm}) => {
  
  function navigate(event)  {
    event.preventDefault();
    dispatch(changeCurrent(event.target.value))
  }
  

  useEffect(() => {
   
  }, [])

  let nbTxtTrou = 0, numQuestion = 0  
  return  (
    <div className="lienQuestion">
      <div className="navLien">
      <form className="form" >
        <div className="btn-group beds-baths-group">
        {Object.keys(qcm).map(id => {
          qcm[id].type === TYPE.TEXTE_A_TROUS && nbTxtTrou++
          return ( qcm[id].type !== TYPE.TEXTE_A_TROUS || nbTxtTrou === 1 ?
          <label className="btn btn-default beds-baths beds-baths-1">
          <button className="bouttonsNav" 
            key={id} 
            value={id} 
            onClick={navigate}
          >
            {++numQuestion}
          </button>
          </label>:null

          )
        })}
        </div>
      </form>
      </div>
    </div>
  )
}

export default connect(null,null)(App);
