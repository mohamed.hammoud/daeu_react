import React from 'react';
import { connect } from "react-redux";
import { changeCurrent, sendReponses } from "../actions/index";
import { englishDone } from "../actions/index";
import { frenchDone } from "../actions/index";
import { Redirect } from 'react-router-dom'
import '../css/Test.css';
import {TYPE} from "./names"





class Qcm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            testDone: false,
            reponsesStagiaire: {},
            btnValiderClicked : false,
        }
        this.previousQuestion = null
        this.next = this.next.bind(this);
        this.previous = this.previous.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.handleRadioChange = this.handleRadioChange.bind(this);
        this.handleSelectChange = this.handleSelectChange.bind(this);
        this.valider = this.valider.bind(this);
    }
    componentDidUpdate(){
        document.querySelectorAll(".questionCheckBox").forEach((domElt) => {
            domElt.after(document.createElement("li"));
        })
    }
    //Removed axios request and passed qcm from Test as props to QCM

    onSubmit(e) {
        e.preventDefault();
    }
   
    next() {
        let { current } = this.props
        const { dispatch, qcm } = this.props
        if (current < Object.keys(qcm).length)
            dispatch(changeCurrent(++current))
    }
    componentWillUnmount(){
        this.valider(true)
    }

   
    valider(event) {
        const { test, dispatch, responses } = this.props
        if(!this.state.btnValiderClicked && event !== true){
                this.setState({ btnValiderClicked: true })
                return    
        }

        event !== true && event.preventDefault()
        
        //Si current c'est anglais lors de la validation on indique dans le store que le test anglais est fini
        //pour l'enlever des choix de test
        test === "Anglais" && dispatch(englishDone())
        test === "Francais" && dispatch(frenchDone())

        //spread copy
        const finalResponses = {...this.state.reponsesStagiaire}

        // change set type to array ( Set disappear when dispatched, not arrray)
        Object.keys(finalResponses).map(key =>
            typeof(finalResponses[key]) === 'object' && (finalResponses[key] = Array.from(finalResponses[key]))
        )

        // concatenate 
        typeof (responses) !== 'undefined' && Object.assign(finalResponses, responses)
           

        dispatch(sendReponses(finalResponses))
        this.setState({ testDone: true }) //pour revenir a choix test
    }

    previous() {
        let { current } = this.props
        const { dispatch } = this.props
        current > 1 && dispatch(changeCurrent(--current))
    }
    // shouldComponentUpdate(nextProps, nextState) {
    //     return nextState.currentQuestion !== this.state.currentQuestion || this.state.currentQuestion === 1 ? true : false

    // }
    handleSelectChange(e) {
        const temp = this.state.reponsesStagiaire
        e.target.value === "" ? delete temp[e.target.name] : temp[e.target.name] = e.target.value
        this.setState({ reponsesStagiaire: temp })

    }

    handleRadioChange(e) {
        const { current } = this.props
        const temp = this.state.reponsesStagiaire
     
        if (e.target.type === "radio")
            temp[current] = e.target.value
        else {
            if (temp[this.props.current] !== undefined) {
                e.target.checked === true ? temp[this.props.current].add(e.target.value) : temp[this.props.current].delete(e.target.value)
            }
            else {
                temp[this.props.current] = new Set([e.target.value])
            }
        }
        this.setState({ reponsesStagiaire: temp })

    }
    getIndex(index){
      
        let cpt = 1, flag = 0
        return Object.keys(this.props.qcm).map(key => {
            if (key !== index){  
                if (this.props.qcm[key].type !== TYPE.TEXTE_A_TROUS){
                    cpt++  
                }
                else{
                    if (flag === 0) flag = 1
                }
            } 
            else {
                return cpt + flag
            }
        })

    }
    render() {
        const {qcm, current } = this.props
        const {btnValiderClicked } = this.state
        if (this.previousQuestion !== current){
            this.previousQuestion = current
            this.setState({ btnValiderClicked: false })
        } 

        if (this.state.testDone === true) {
            return (
                <Redirect to="/Choix" />
        )
            }
        if (Object.keys(qcm).length > 0 && current > 0) {
            if (qcm[current].type === TYPE.QCM) {
                return (
                    <div>
                        <form className="form" onSubmit={(e) => this.onSubmit(e)} >
                            <div className='questionBox'>
                                <h4>{qcm[current].consigne}</h4>
                                <div className='questions'>
                                    Question {this.getIndex(current)} : {qcm[current].libelle}
                                    <ul className='answerList'>
                                        {Object.keys(qcm[current].reponses).map((k) => {
                                            const reponse = qcm[current].reponses[k]
                                            return (
                                                < li key={"q" + current + "r" + k} className="custom-control custom-radio" >
                                                    <input
                                                        id={"q" + current + "r" + k}
                                                        type="radio"
                                                        className="custom-control-input"
                                                        name={"q" + current}
                                                        value={reponse.idrep}
                                                        checked={this.state.reponsesStagiaire[current] === reponse.idrep}
                                                        onChange={(e) => this.handleRadioChange(e)}
                                                    />
                                                    <label className="custom-control-label" htmlFor={"q" + current + "r" + k}>{reponse.libelle}</label>
                                                </li>

                                            )

                                    })}

                                </ul>
                            </div>
                            </div>
                        </form>
                        <div className="container">
                                    <div className="rows">
                                        {/* <button onClick={this.previous} className="btn-change8">Question précédente</button>
                                        <button onClick={this.next} className="btn-change7">Question suivante</button> */}
                                        <button onClick={this.valider} className="btn-change6 valider">{btnValiderClicked ? 'Cliquez une deuxième fois pour confirmer la fin du test' : 'Terminer le test'}</button>
                                    </div>
                        </div>
                        <footer className="footer"> A voir avec les profs</footer>
                    </div>
                    

                )
            }
            else if (qcm[current].type === "Texte a faute") {
                return (
                    <div>
                        <form className="form" onSubmit={(e) => this.onSubmit(e)} >
                            <div className='questionBox'>
                                <h4>{qcm[current].consigne}</h4>
                                <div className='questions'>
                                    Question {this.getIndex(current)} : < br/>
                                    {qcm[current].libelle.split(/Ligne /).map((ligne, index) => {
                                                return (index > 0 ? <p key={index}> Ligne {ligne} < br/></p> : null)
                                            })}
                                    <ul className='answerList-special'>
                                        {
                                        Object.keys(qcm[current].reponses).map((k) => {
                                          
                                            
                                            const reponse = qcm[current].reponses[k];

                                            console.log(reponse.libelle + " | " + reponse.libelle.substr(reponse.libelle.length - 1))
                                            return (
                                                
                                                <p key={"q" + current + "r" + k} className={reponse.libelle.substr(reponse.libelle.length - 1) === '.' ? 'questionCheckBox' : 'bb' }>
                                                    <input
                                                        type="checkbox"
                                                        id={"q" + current + "r" + k}
                                                        className="cust-contrspecial"
                                                        name={"q" + current}
                                                        value={reponse.idrep}
                                                        checked={this.state.reponsesStagiaire[current] !== undefined ? this.state.reponsesStagiaire[current].has(reponse.idrep) : false}
                                                        onChange={(e) => this.handleRadioChange(e)}
                                                    />
                                                    
                                                    <label htmlFor={"q" + current + "r" + k}>{reponse.libelle}</label>
                                                </p>
                                                
                                            )
                                    })}
                                </ul>
                            </div>
                            </div>
                        </form>
                        <div className="container">
                            <div className="rows">
                                {/* <button onClick={this.previous} className="btn-change8">Question précédente</button>
                                <button onClick={this.next} className="btn-change7">Question suivante</button> */}
                                <button onClick={this.valider} className="btn-change6 valider">{btnValiderClicked ? 'Cliquez une deuxième fois pour confirmer la fin du test' : 'Terminer le test'}</button>
                            </div>
                        </div>
                        <footer className="footer"> A voir avec les profs</footer>
                    </div>
                )
            }
            else if (qcm[current].type === TYPE.TEXTE_A_TROUS) {
                const start = parseInt(Object.keys(qcm)[0])
                const end = parseInt(Object.keys(qcm).length) + start
                const buffer = []
                for (let id = start; id < end; id++) {
                    if (qcm[id].type === TYPE.TEXTE_A_TROUS) {
                        buffer.push(
                            <div className="divTestFr">
                            <p key={id} className="testFr">
                                {qcm[id].libelle}
                                {qcm[id].reponses.length > 0
                                    ?
                                    id === parseInt(current)
                                        ?
                                        <select
                                            className="form-control selectFr"
                                            onChange={this.handleSelectChange}
                                            name={id}
                                            defaultValue={this.state.reponsesStagiaire[id]}
                                        >
                                            <option />
                                            {qcm[id].reponses.map(rep =>
                                                <option
                                                    key={rep.idrep}
                                                    // selected={this.state.reponsesStagiaire[id] === rep.idrep} ==> USAGE REACT : Utiliser defaultValue du parent
                                                    value={rep.idrep}
                                                >
                                                    {rep.libelle}
                                                </option>)}
                                        </select>

                                        :
                                        <select
                                            className="form-control selectFr container-fluid"
                                            onChange={this.handleSelectChange}
                                            name={id}
                                            defaultValue={this.state.reponsesStagiaire[id]}
                                        >
                                            <option />
                                            {qcm[id].reponses.map(rep =>
                                                <option
                                                    key={rep.idrep}
                                                    // selected={this.state.reponsesStagiaire[id] === rep.idrep} ==> USAGE REACT : Utiliser defaultValue du parent
                                                    value={rep.idrep}
                                                >
                                                    {rep.libelle}
                                                </option>)}
                                        </select> : null}
                            </p>
                            </div>
                        )

                    }

                }
                return (
                    <div>
                        <h3 className="consigneFr">Compléter le texte ci-dessous avec l'orthographe exacte, dans chaque case blanche cliquez sur celle-ci pour sélectionner la réponse :</h3>
                        {buffer}
                        <br/>
                        <div className="row justify-content-center">
                            <button onClick={this.valider} className="btn-change6 valider">{btnValiderClicked ? 'Cliquez une deuxième fois pour confirmer la fin du test' : 'Terminer le test'}</button>
                        </div>
                    </div>
                )
            }
        }
    }
}

const mapStateToProps = (state) => {
    return {
        current: state.current,
        test: state.test,
        responses: state.responses,
    }
};


export default connect(mapStateToProps, null)(Qcm);

