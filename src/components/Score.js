import React from 'react';
import { useEffect, useState } from "react";
import { connect } from "react-redux";
import { API } from "./names"
import axios from "axios";
import '../css/Test.css';
import checked from '../images/checked.png'


export const Score = ({ id, responses , choix }) => {
  const [resultatAnglais, setResultatAnglais] = useState(0)
  const [resultatFrancais, setResultatFrancais] = useState(0)
  const [voireScore, setVoirScore] = useState(false)


  useEffect(() => {

    axios.post(API, { type: 'showscore' }) //SI on met choix ca va tjrs recupere les memes qcm pour anglais et francais
      .then(response => response.data)
      .then(response => {
        console.log(response)
        setVoirScore(response['validity'])
      })

    responses.type = "calculScore"
    responses.token = id
    console.log(responses)
    axios.post(API, responses, { withCredentials: true }) //SI on met choix ca va tjrs recupere les memes qcm pour anglais et francais
      .then(response => response.data)
      .then(response => {
        setResultatAnglais(response['Anglais'])
        setResultatFrancais(response['Francais'])
      })

  }, [])


  return (
    <div>
      {voireScore === true ?
        <div>
          <div>Votre resultat à l'exam de Francais est {resultatFrancais}</div>
          {choix === 'Anglais' ?
          <div>Votre resultat dans l'exam d'Anglais est {resultatAnglais}</div>
          :null}
        </div>
        :
        <div className="scoreTxt">
          <p>Examen terminé</p>
          <img className="checkedMarks" src={checked} alt=""/>
          <p>Vous pouvez quitter.</p>
      </div>
      }
    </div>
  );
}


const mapStateToProps = (state) => {
  return {
    choix : state.choix,
    responses: state.responses,
    id: state.id
  }
};


export default connect(mapStateToProps, null)(Score);
