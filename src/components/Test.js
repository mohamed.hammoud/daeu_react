import React from 'react';
import Navigation from './Navigation';
import Qcm from './Qcm';
import { connect } from "react-redux";
import { API } from "./names"
import axios from "axios";
import { changeCurrent } from "../actions/index";
import Chrono from './Chrono';
import { Redirect } from 'react-router-dom';



class Test extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      qcm: {},
      durations: {}, 
      testTermine : false
    }
    this.endFunc = this.endFunc.bind(this)
  }

  componentDidMount() {
    const { dispatch, langue } = this.props

    axios.post(API, { type: 'getQcm', langue: langue }) //SI on met choix ca va tjrs recupere les memes qcm pour anglais et francais
      .then(response => response.data)
      .then(response => {
        dispatch(changeCurrent(Object.keys(response)[0])) //Sinon ca va cause des problemes au niveau du 2eme test car ca fait un erreur
        //car ca essaye de render avec qcm avant de modifier le current dans le store
        this.setState({ qcm: response })

      })
    axios.post(API, { type: 'getTestsDuration', langue: langue })
    .then(response => response.data)
    .then(response => {
      console.log(response)
      this.setState({durations : response})
    })

      
    axios.post(API, { type: 'getTestsDuration', langue: langue })
      .then(response => response.data)
      .then(response => {
        console.log(response)
        this.setState({durations : response})
      })
  }
  //passage de QCM en props a Test
  endFunc(){
    console.log('fin du timer..')
    this.setState({ testTermine : true})
  }


  //IDEE POUR TEXTE A TROU SEPARE EN 3 COMPONENTS POUR CHAQUE TYPE DE QUESTION PASSE QCM EN PARAM ET INDIQUER LE NUMERO DE PARTIE
  //POUR RECUPERE LES QUESTIONS NECESSAIRES
  render() {
    const { qcm, durations, testTermine } = this.state
    return (

      <div>
        {Object.keys(qcm).length > 0 && Object.keys(durations).length > 0 ?
          !testTermine 
          ?
            <div>
              
              <Chrono time={durations[this.props.langue]} endFunc={this.endFunc} className="chrono"></Chrono>
              <Navigation range={Object.keys(qcm)} qcm = {qcm} />
              <div className="borderTxtFr">
                <Qcm qcm={qcm}  />
              </div>
            </div>
          :
            <Redirect to="/Choix"/>
          :
          <span className="spinner-border" role="status" aria-hidden="true"></span>
        }
      </div>
    );

  }
}

const mapStateToProps = (state) => {
  return {
    choix: state.choix
  }
};

export default connect(mapStateToProps, null)(Test);
