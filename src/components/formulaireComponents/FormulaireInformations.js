import React from 'react';
import {Field } from "react-final-form";
import './FormulaireInformations.css';
export class FormulaireInformations extends React.Component {
    state = {

    }
    onSubmit = this.onSubmit.bind(this)

    onSubmit(e){
    }
  
    render() {
        return (
            <React.Fragment>
                <h2 className="information">Informations vous concernant</h2>
                <div className="form-group input-group mb-3">
                    <div className="input-group-prepend">
                        <span className="input-group-text">Nom</span>
                    </div>
                    <Field
                        name='nom'
                        component='input'
                        type='text'
                        required='required'
                        className='form-control'
                        value=""
                    />
                </div>

                <div className="form-group input-group mb-3">
                    <div className="input-group-prepend">
                        <span className="input-group-text">Prénom</span>
                    </div>
                    <Field
                        name='prenom'
                        component='input'
                        type='text'
                        required='required'
                        maxLength='100'
                        className='form-control'
                        value=""
                    />
                </div>

                <div className="form-group input-group mb-3">
                    <div className="input-group-prepend">
                        <span className="input-group-text">Date Naissance</span>
                    </div>
                    <Field
                        name='dateNaissance'
                        component='input'
                        type='date'
                        required='required'
                        className='form-control'
                    />
                </div>

                <div className="form-group input-group mb-3">
                    <div className="input-group-prepend">
                        <span className="input-group-text">Email</span>
                    </div>
                    <Field
                        name='email'
                        component='input'
                        type='email'
                        maxLength='100'
                        className='form-control'
                    />
                </div>
                </React.Fragment>


        )
    }
}
