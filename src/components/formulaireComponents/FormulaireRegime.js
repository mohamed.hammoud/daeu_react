import React from 'react';
import { Field } from 'react-final-form'



export const FormulaireRegime = () => {

   

    return (
        <React.Fragment>
            <div className="form-group input-group mb-3">
                <div className="input-group-prepend">
                    <span className="input-group-text">Régime</span>
                </div>
                <Field name="regime" component="select" className='form-control' required>
                    <option></option>
                    <option value="soir">Cours du soir</option>
                    <option value="sed">Sed</option>
                    <option value="null">Je ne sais pas encore</option>

                </Field>
            </div>
            <div className="form-group input-group mb-3">
                <div className="input-group-prepend">
                    <span className="input-group-text">Langue</span>
                </div>
                <Field name="langue" id="choix" component="select" className='form-control' required>
                    <option></option>
                    <option value="Anglais">Anglais</option>
                    <option value="Francais">Francais</option>
                    <option value="Francais">Espagnol</option>
                </Field>
            </div>


        </React.Fragment>
    )
}



export default FormulaireRegime;
