export const API =  "http://localhost/daeu/api.php"
export const TYPE = {
    QCM : 'QCM',
    TEXTE_A_TROUS : 'Texte a trous',
    TEXTE_A_FAUTES : 'Texte a faute'
}
// export const API = "http://127.0.0.1/edsa-DAEU/phpQueries/api.php"; //Pierre : "http://127.0.0.1/edsa-DAEU/phpQueries/api.php" Autres : "http://localhost/daeu/api.php"
export const WRONG_CODE = "Le code donné ne correspond à aucune session";
export const CONSIGNE = [
    "Le test s'effectue sans interruption possible. ",
    "La validation sera faite automatiquement au bout de ce délai. ",
    "Un chronomètre sera sur la page pour vous informer du temps restant. ",
    "Après avoir répondu aux questions, cliquez sur partie suivante pour accéder au deuxième exercice. ",
    "Une fois ceci terminé, cliquez sur \"Envoyer réponses\" pour terminer votre test. ",
    "Vous pouvez revenir aux questions auxquelles vous n'avez pas répondu."
]
    
    
    