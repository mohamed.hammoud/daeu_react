
const initialState = [];
export const reducer = (state = initialState, action) => {
      switch (action.type) {

            case "CHANGE":
                  return {
                        ...state,
                        current: action.current
                  };

            case "ENGLISH":
                  return {
                        ...state,
                        englishDone: true
                  };

            case "FRENCH":
                  return {
                        ...state,
                        frenchDone: true
                  };

            case "CHOIX":
                  return {
                        ...state,
                        choix: action.choix
                  };

            case "TEST":
                  return {
                        ...state,
                        test: action.test
                  }

            case "SENDRESPONSES":
                  return {
                        ...state,
                        responses: action.responses
                  }

            case "STOREID":
                  return {
                        ...state,
                        id: action.id
                  };

            default:
                  return state
      }
}

export default reducer;
